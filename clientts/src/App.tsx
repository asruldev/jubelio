import React from 'react';
// import { ToastContainer } from "react-toastify";
import ProductList from './Product/List';

const App: React.FC = () => {
  return (
    <>
      <div className="App">
        <ProductList />
      </div>
      {/* <ToastContainer
        position="bottom-center"
        autoClose={5000}
        hideProgressBar={false}
        newestOnTop={false}
        closeOnClick
      /> */}
    </>
  );
}

export default App;
