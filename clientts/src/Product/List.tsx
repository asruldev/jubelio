import React, { useEffect, useState } from "react";
import { observer } from "mobx-react";
import { useStores } from "../stores/useStores";
import "./List.css"
import { callapi } from "callapi";
import Axios from "axios";

const ProductList = observer(() => {
    const { productStore } = useStores();
    const [editId, setEditId] = useState(null);
    const [dataEdit, setDataEdit] = useState({
        nama: "",
        sku: "",
        gambar: "",
        harga: 0
    });

    useEffect(() => {
        callapi.get('products')
        .then( resp => {
            productStore.initProducts(resp)
        })
        
    }, [productStore])

    const getImage = (no: any) => {
        Axios.get('products-img', { params: {no: no} })
        .then( resp => console.log(resp))
    }

    const formatRupiah = (money: number): any => {
        return new Intl.NumberFormat('id-ID', { style: 'currency', currency: 'IDR' }).format(money);
    }

    const deleteProduct = (id: any) => {
        callapi.del(`products/${id}`)
        .then( resp => {
            productStore.deleteProduct(id)
        })
    }

    const changeNama = (e: React.ChangeEvent<HTMLInputElement>): void => {
        e.persist()
        setDataEdit(t => {
            console.log({...t, nama: e.target.value})
            return {...t, nama: e.target.value}
        })
    }

    const changeHarga = (e: React.ChangeEvent<HTMLInputElement>): void => {
        e.persist()
        setDataEdit(t => {
            return {...t, harga: +e.target.value}
        })
    }

    const updateData = () => {
        callapi.put(`products/${editId}`, dataEdit)
        .then( resp => {
            let newData: any = {...dataEdit}
            newData.id = editId
            productStore.updateProduct(editId, newData)
            setDataEdit({
                nama: "",
                sku: "",
                gambar: "",
                harga: 0
            })

            setEditId(null)
        })
    }

    return (
        <>
            <h2>Products</h2>

            <section className="products">
                {
                    productStore?.products && productStore?.products.map( (item: any, index: any) => {
                        return (
                            <div className="product-card" key={index}>
                                <div className="product-image">
                                    <img src={item.gambar} alt={item.gambar} 
                                    onError={(e: any)=>{e.target.onerror = null; e.target.src="https://sunattanpasuntik.com/wp-content/themes/Khitan-modern-tanpa-jarum-suntik/assets/images/blog/no-img.jpg"}}
                                    />
                                </div>
                                <div className="product-info">
                                    { editId === item.id
                                        ? <label>Nama: <input type="text" value={dataEdit.nama} onChange={ e => changeNama(e)}/></label>
                                        : <h5>{item.nama}</h5>
                                    }

                                    { editId === item.id
                                        ? <label>Harga: <input type="number" value={dataEdit.harga} onChange={ e => changeHarga(e)}/></label>
                                        : <h6>{formatRupiah(item.harga)}</h6>
                                    }
                                </div>
                                <div>
                                    { editId === item.id
                                    ? <button onClick={updateData}>update</button>
                                    : <button onClick={() => {
                                        setEditId(item.id) 
                                        setDataEdit({
                                            nama: item.nama,
                                            sku: item.sku,
                                            gambar: item.gambar,
                                            harga: item.harga
                                        })
                                    }}>Edit</button>
                                    }
                                    <button onClick={(e) => deleteProduct(item.id)}>Delete</button>
                                </div>
                            </div>
                        )
                    })
                }
            </section>
        </>
    );
});

export default ProductList