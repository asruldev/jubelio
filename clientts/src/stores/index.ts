import { createContext } from "react";
import { ProductStore } from "./ProductStore";

interface IStore {
    productStore: any;
}

export const rootStoreContext = createContext<IStore>({
    productStore: new ProductStore()
});
