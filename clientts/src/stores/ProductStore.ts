import { decorate, observable } from "mobx";

export interface IProduct {
    id: any;
    nama: string;
    sku: any;
    gambar: string;
    harga: number;
}

export class ProductStore {
    public products: IProduct[] = [];

    public initProducts = (products: IProduct[]) => {
        this.products = this.products.concat(...products)
    };

    public addProduct = (product: IProduct) => {
        this.products.push(product);
    };

    public updateProduct = (id: any, updateProduct: IProduct) => {
        // const updateProducts = this.products.map(item => {
        //     if (item.id === id) {
        //         updateProduct.id = id
        //         return { ...updateProduct };
        //     }
        //     return item;
        // });
        // this.products = updateProducts;

        const updateProducts: any = this.products.filter((item:any) => item.id !== id);

        this.products = [updateProduct, ...updateProducts]
    };

    public deleteProduct = (id: any) => {
        const updateProducts = this.products.filter(item => item.id !== id);
        this.products = updateProducts;
    };
}

decorate(ProductStore, {
    products: observable,
});
