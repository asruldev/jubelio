"use strict";
const dotenv = require("dotenv")
const axios = require("axios")
const parser = require("xml2json")
const model = require('../models')

const optionjson = {
    object: true,
    reversible: false,
    coerce: false,
    sanitize: true,
    trim: true,
}

const home = {
    method: "GET",
    path: "/",
    handler: (request, h) => {
        return "hello world!";
    }
};

const getDataElevania = async (page) => {
    try {
        let { data } = await axios.get(`${process.env.API_URL}/product/listing?page=${page}`, {
            headers: {
                openapikey: process.env.API_KEY
            }
        })
        let arr = []
        let hasil = await parser.toJson(data, optionjson).Products.product
        hasil.map( async item => {
            const nama = item.prdNm
            const sku = item.prdNo
            const gambar = item.prdNo
            const harga = item.selPrc
            arr.push({nama, sku, gambar, harga})
            model.Product.create({nama, sku, gambar: await getPicture(gambar), harga})
        })
        return arr;
    } catch (error) {
        return "Error ksini"
    }
}

const getPicture = async (no) => {
    try {
        let { data } = await axios.get(`${process.env.API_URL}/product/details/${no}`, {
            headers: {
                openapikey: process.env.API_KEY
            }
        })
        let hasil = await parser.toJson(data, optionjson).Product.prdImage01
        console.log(hasil)
        return hasil;

    } catch (error) {
        return "Error ksini"
    }
};

const initdata = {
    method: "GET",
    path: "/data-product",
    handler: (request, h) => {
        const page = request.query.page ? request.query.page : 1
        let hasil = getDataElevania(page)
        return hasil
    }
};

const getdata = {
    method: "GET",
    path: "/products",
    handler: (request, h) => {
        return model.Product.findAll()
    },
    config: {
        cors: {
            origin: ['*'],
        }
    },
};

const getImage = {
    method: "GET",
    path: "/products-img",
    handler: async (request, h) => {
        const no = request.query.no
        try {
            let { data } = await axios.get(`${process.env.API_URL}/product/details/${no}`, {
                headers: {
                    openapikey: process.env.API_KEY
                }
            })
            let hasil = await parser.toJson(data, optionjson).Product.prdImage01
            return hasil;
        } catch (error) {
            return "Error ksini"
        }
    },
    config: {
        cors: {
            origin: ['*'],
        }
    },
};

const putdata = {
    method: "PUT",
    path: "/products/{id}",
    handler: async (request, h) => {
        const product = await model.Product.findOne({where:{id: request.params.id}})
        product.update(request.payload)

        return product.save()
    },
    config: {
        cors: {
            origin: ['*'],
        }
    },
};

const deletedata = {
    method: "DELETE",
    path: "/products/{id}",
    handler: async (request, h) => {
        return model.Product.destroy({where:{id: request.params.id}})
    },
    config: {
        cors: {
            origin: ['*'],
        }
    },
};

module.exports = [home, initdata, getdata, putdata, deletedata, getImage];